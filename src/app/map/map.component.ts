import { Component, OnDestroy, OnInit, Input } from '@angular/core';
import * as Leaflet from 'leaflet';
import { DataserviceService } from 'src/app/common/services/dataservice.service';
import { CommunesserviceService } from 'src/app/common/services/communes.service';
import { Communes } from 'src/app/common/data/communes';
import { Datatourisme } from 'src/app/common/data/datatourisme';
import { DatatourismeService } from 'src/app/common/services/datatourisme.service';

declare var ol: any;

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit, OnDestroy {
  /*activities=[
    { name:"cyclotourisme", acti:"CyclingTour"   },
    { name:"Balade marche", acti:"WalkingTour"   },
    { name:"Evenements Culturel", acti:"CulturalEvent"   }
    
  ];*/
  Codedepartement: any;
  private longitude: any;
  private latitude: any;
  //villesSelect:Communes;
  commune: String = "";
  title = 'yakoiou-app';
  map: Leaflet.Map;
  _listeActivites: Datatourisme[];
  Activity: String;
  markerGroup: any;
  constructor(private DataSharing: DataserviceService, private api: CommunesserviceService, private apiTour: DatatourismeService) {
    this.DataSharing.SharingData.subscribe((res: any) => {
      this.commune = res; this.getCommuneByName(res);
    });
    this.DataSharing.Activite.subscribe((res: any) => {
      this.Activity = res;
    });
  }
  //recentrer la carte sur le lieu spécifié
  setCenter() {
    this.map.panTo(new Leaflet.LatLng(this.latitude, this.longitude));
    this.map.setZoom(10);
  }
  getActivitiesByDepartetement(codeDepartement: String, typeActivite: String) {
    this.apiTour.getActivitiesTypeByDepartement$(codeDepartement, typeActivite).subscribe(

      {
        next: (datatourDep: any): void => {
          this._listeActivites = (datatourDep);
        },
        error: (err) => { console.log("error : " + err); }
      });
  }

  selectionItem(Choix: Number) {
    switch (Choix) {
      case 1: {
        this.getActivitiesByDepartetement(this._InfosCommune.code_departement, "Forest");
        this.DataSharing.Activite.next("Forest");
        this.leafletMap('red');
        break;
      }
      case 2: {
        this.getActivitiesByDepartetement(this._InfosCommune[0].code_departement, "schema:Landform");
        this.DataSharing.Activite.next("Rambling");
        this.leafletMap('bleu');
        break;
      }
      default: {
        this.getActivitiesByDepartetement(this._InfosCommune[0].code_departement, "CulturalEvent");
        this.DataSharing.Activite.next("Visit");
        this.leafletMap('green');
        break;
      }
    }
    //this.leafletMap();
  }

  ngOnInit(): void {

    this.map = Leaflet.map('map', {
      center: [43.95, 4.8167],
      zoom: 13
    });
    Leaflet.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
      maxZoom: 18
    }).addTo(this.map);

    // Leaflet.marker([43.95, 4.8167]).addTo(this.map).bindPopup('Avignon').openPopup();
  }
  _InfosCommune = new Communes();

  getCommuneByName(communes) {
    this.api.getDetailsCommuneByName$(communes).subscribe(
      (dataCommune: Communes) => { this._InfosCommune = dataCommune; this.Codedepartement = dataCommune[0].code_departement; this.DataSharing.Codedepartement.next(this.Codedepartement);this.latitude = Number(dataCommune[0].latitude); this.longitude = Number(dataCommune[0].longitude); console.log(dataCommune[0].longitude); console.log(dataCommune); this.setCenter(); }

    );
    //if (this.Codedepartement.length<2){this.Codedepartement='0'+this.Codedepartement;}
    
  }
  leafletMap(colormarker: string): void {
   // this.markerGroup.clearLayers();
    this.markerGroup = Leaflet.layerGroup().addTo(this.map)
    for (const property of this._listeActivites) {
      let lat = Number(property.isLocatedAt[0]['schema:geo']['schema:latitude']);
      let lon = Number(property.isLocatedAt[0]['schema:geo']['schema:longitude']);
     let titre = JSON.stringify(property['rdfs:label'].fr).split("[").join(" ").split("]").join(" ");
     //let titre="<br><p>'{{property['hasDescription']}}'</p>"
      //  Leaflet.marker([property.isLocatedAt[Symbol], property.long]).addTo(this.map)
      Leaflet.marker([lat, lon]).
        addTo(this.markerGroup).
        addTo(this.map).

        bindPopup(titre).
        openPopup();
      // .bindPopup(property['rdfs:label'].fr)
      // .openPopup();
    }

  }
  ngOnDestroy(): void {
    this.map.remove();
  }
}
