import { Component, OnInit } from '@angular/core';
import {AuthService } from 'src/app/common/services/authservice.service';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss']
})
export class LogoutComponent implements OnInit {

  constructor(private authService :AuthService, private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
this.authService.logout();
const returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
      this.router.navigateByUrl(returnUrl);
  }

}
