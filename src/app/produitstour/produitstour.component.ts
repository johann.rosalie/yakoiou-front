import { Component, OnInit } from '@angular/core';
import {Produits}from 'src/app/common/data/produits';
import {ProduitsService} from 'src/app/common/services/produits.service';
import { CommunesserviceService } from 'src/app/common/services/communes.service';
import { Communes } from 'src/app/common/data/communes';
import { Datatourisme } from 'src/app/common/data/datatourisme';
import { DataserviceService } from 'src/app/common/services/dataservice.service';
import { DatatourismeService } from 'src/app/common/services/datatourisme.service';

@Component({
  selector: 'app-produitstour',
  templateUrl: './produitstour.component.html',
  styleUrls: ['./produitstour.component.scss']
})
export class ProduitstourComponent implements OnInit {
listProduit:Produits[];
  constructor(private DataSharing: DataserviceService,private produitsService:ProduitsService){}
  ngOnInit(): void {
   this.produitsService.getAllProduitsEnable$().subscribe({
    next: (response:Produits[]): void => {this.listProduit=response},
    error: (err) => { console.log("error : " + JSON.stringify(err)); }
   });

}

/*selectionItem(Choix: Number) {
  switch (Choix) {
    case 1: {
      this.getActivitiesByDepartetement(this._InfosCommune.code_departement, "SportsAndLeisurePlace");
      this.DataSharing.Activite.next("SportsAndLeisurePlace");
     // this.leafletMap('red');
      break;
    }
    case 2: {
      this.getActivitiesByDepartetement(this._InfosCommune[0].code_departement, "WalkingTour");
      this.DataSharing.Activite.next("WalkingTour");
      
      break;
    }
    default: {
      this.getActivitiesByDepartetement(this._InfosCommune[0].code_departement, "CulturalEvent");
      this.DataSharing.Activite.next("CulturalEvent");
      //this.leafletMap('green');
      break;
    }
  }
  //this.leafletMap();
}*/
}