import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProduitstourComponent } from './produitstour.component';

describe('ProduitstourComponent', () => {
  let component: ProduitstourComponent;
  let fixture: ComponentFixture<ProduitstourComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProduitstourComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProduitstourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
