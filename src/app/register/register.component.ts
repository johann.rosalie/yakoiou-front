import { Component, OnInit, TemplateRef } from '@angular/core';
import {Register} from '../common/data/register';
import {UserInfosService} from '../common/services/userinfos.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent  {
 
  constructor(private userInfosService:UserInfosService,private route: ActivatedRoute,
    private router: Router,private location:Location) { }

   public register = new Register();
   public message:String;
  
  onRegister(){
    this.userInfosService.RegisterUserInfosService$(this.register).subscribe({
      next: (response:any): void => { this.message = JSON.stringify(response);alert("Success") ;const returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/nrg-login';
      this.router.navigateByUrl(returnUrl) },
      error: (err) => {this.message = err;alert("error : " + JSON.stringify(err)); }

    });
  }
  
  goBack(): void {
    this.location.back();
  }
}