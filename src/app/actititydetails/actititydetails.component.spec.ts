import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActititydetailsComponent } from './actititydetails.component';

describe('ActititydetailsComponent', () => {
  let component: ActititydetailsComponent;
  let fixture: ComponentFixture<ActititydetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActititydetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActititydetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
