import { Component, OnInit } from '@angular/core';
import { User} from '../common/data/user';
import {UserInfosService} from '../common/service/user-infos.service'
import { Observable, of } from 'rxjs';

@Component({
  selector: 'app-user-infos',
  templateUrl: './user-infos.component.html',
  styleUrls: ['./user-infos.component.scss']
})
export class UserInfosComponent implements OnInit {
   listUserInfos : User[];
   listeroles :string[];
  constructor(private _userInfosService : UserInfosService ) {   
   }
    initListUser(tabUser : User[]){
      this.listUserInfos=tabUser;
    }

  ngOnInit(): void {
    this._userInfosService.getAllUserInfosService$()
    .subscribe({
      next: (tabUser: User[])=>{ this.initListUser(tabUser); },
      error: (err) => { console.log("error:"+err)}
   });
}


     
}
