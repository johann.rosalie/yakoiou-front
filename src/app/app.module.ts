import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { HeaderComponent } from './header/header.component';
import { MapComponent } from './map/map.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { MyAuthInterceptor } from './common/interceptor/my-auth-interceptor';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import { RegisterComponent } from './register/register.component';

//import { UserInfosComponent } from 'src/app/usersinfos/usersinfos.component';

import { HomeComponent } from './home/home.component';
import { MeteodetailsComponent } from 'src/app/meteodetails/meteodetails.component';

import { ActivitiestourComponent } from './activitiestour/activitiestour.component';
import {DataserviceService} from 'src/app/common/services/dataservice.service';
import { NgxTypeaheadModule } from 'ngx-typeahead';       
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { UsersinfosComponent} from 'src/app/usersinfos/usersinfos.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { AlertModule,AlertConfig } from 'ngx-bootstrap/alert';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { BsDropdownModule,BsDropdownConfig } from 'ngx-bootstrap/dropdown';
import { ModalModule, BsModalService } from 'ngx-bootstrap/modal';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { PaginationModule,PaginationConfig } from 'ngx-bootstrap/pagination';
import * as Leaflet from 'leaflet';
import { ProduitstourComponent } from './produitstour/produitstour.component';
import { ManagerComponent } from './manager/manager.component';
import { LogoutComponent } from './logout/logout.component';
import { ActititydetailsComponent } from './actititydetails/actititydetails.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
     MapComponent,
     UsersinfosComponent,
     HomeComponent,
     MeteodetailsComponent,
     ActivitiestourComponent,
     UsersinfosComponent,
     RegisterComponent,
     UserProfileComponent,
     ProduitstourComponent,
     ManagerComponent,
     LogoutComponent,
     ActititydetailsComponent,
     

    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,FormsModule, BrowserAnimationsModule,HttpClientModule, NgxTypeaheadModule,BrowserModule,
    AccordionModule,
    AlertModule,
    ButtonsModule,CarouselModule, FormsModule ,AccordionModule.forRoot(),
    CollapseModule,ReactiveFormsModule,
    BsDropdownModule,TypeaheadModule,PaginationModule.forRoot(),
    ModalModule.forRoot()
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
  useClass: MyAuthInterceptor,
  
    multi: true,
    
     }],
  bootstrap: [AppComponent]
})
export class AppModule { }
