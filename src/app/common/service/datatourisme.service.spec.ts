import { TestBed } from '@angular/core/testing';

import { DatatourismeService } from './datatourisme.service';

describe('DatatourismeService', () => {
  let service: DatatourismeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DatatourismeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
