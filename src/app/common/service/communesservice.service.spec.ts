import { TestBed } from '@angular/core/testing';

import { CommunesserviceService } from './communesservice.service';

describe('CommunesserviceService', () => {
  let service: CommunesserviceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CommunesserviceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
