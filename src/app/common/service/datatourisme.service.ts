import { Injectable } from '@angular/core';
import { from ,Observable} from 'rxjs';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import {catchError} from 'rxjs/operators';
import {Datatourisme} from 'src/app/common/data/datatourisme';

const  API_URL= environment.apiNodeUrl;
@Injectable({
  providedIn: 'root'
})
export class DatatourismeService {

  private _apiBaseUrl = API_URL;
  private _headers = new HttpHeaders({'Access-Control-Allow-Origin':'*','Content-Type': 'application/json'  });
  constructor(private http: HttpClient) { }
  getAllactivitiesByDepartement$(departement:String):Observable<Datatourisme[]>
  {
    const headers=new HttpHeaders().set("Content-Type","application/json");
    return this.http.get<Datatourisme[]>(API_URL+ "//tourisme/public/activitiesDepartement/"+departement,{ headers}).pipe(catchError(this.handleError));
  }
  getActivityTypeByCommnune$(nomCommune:String, typeActivite:String,codePostal:String):Observable<Datatourisme[]>
  {
    const headers=new HttpHeaders().set("Content-Type","application/json");
    return this.http.get<Datatourisme[]>(API_URL+ '/tourisme/public/activiteCommnune?'+'nomCommune='+nomCommune +'&typeActivite='+typeActivite + '&codePostal='+codePostal,{ headers}).pipe(catchError(this.handleError));
  }
  getActivitiesTypeByDepartement$(codeDepartement:String,typeActivite:String):Observable<Datatourisme[]>{
    const headers=new HttpHeaders().set("Content-Type","application/json");
    return this.http.get<Datatourisme[]>(API_URL+'/tourisme/public/activiteDepartement?' +'codeDepartement=' +codeDepartement +'&typeActivite='+typeActivite ,{ headers}).pipe(catchError(this.handleError));
  }

  getActivityById$(_id:string):Observable<Datatourisme>{
    const headers=new HttpHeaders().set("Content-Type","application/json");
    return this.http.get<Datatourisme>(API_URL+ '/tourisme/public/activitiesById/'+_id ,{ headers}).pipe(catchError(this.handleError));
  }
  private handleError(error: Response|any){
    return Observable.throw(error);
  }
}
