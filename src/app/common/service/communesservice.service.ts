import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import {Communes} from 'src/app/common/data/communes';
import { environment } from 'src/environments/environment';
import {catchError} from 'rxjs/operators';

const  API_URL= environment.apiNodeUrl;
@Injectable({
  providedIn: 'root'
})
export class CommunesserviceService {
  private _apiBaseUrl = API_URL;
  private _headers = new HttpHeaders({'Access-Control-Allow-Origin':'*','Content-Type': 'application/json'  });
  constructor(private http: HttpClient) { }
  getDetailsAllCommunes$():Observable<Communes[]>
  {
    const headers=new HttpHeaders().set("Content-Type","application/json");
    return this.http.get<Communes[]>(API_URL+ '/communes/public/Allcommunes',{ headers}).pipe(catchError(this.handleError));
  }
  getDetailsCommuneByName$(commune:string):Observable<Communes>{
    const headers=new HttpHeaders().set("Content-Type","application/json");
    return this.http.get<Communes>(API_URL+ '/communes/public/commune?nomCommune='+ commune ,{ headers}).pipe(catchError(this.handleError));
  }
  getDetailsCommuneByCodePostal$(codePostal:string):Observable<Communes>{
    const headers=new HttpHeaders().set("Content-Type","application/json");
    return this.http.get<Communes>(API_URL+ '/communes/public/commune?codePostal='+ codePostal ,{ headers}).pipe(catchError(this.handleError));
  }
  getDetailsCommuneByCodeInsee$(codeInsee:string):Observable<Communes>{
    const headers=new HttpHeaders().set("Content-Type","application/json");
    return this.http.get<Communes>(API_URL+ '/communes/public/commune?codeInsee='+ codeInsee ,{ headers}).pipe(catchError(this.handleError));
  }
  private handleError(error: Response|any){
    return Observable.throw(error);
  }
}
