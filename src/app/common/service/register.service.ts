import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { tap  } from 'rxjs/operators';
import {environment} from 'src/environments/environment';
const apiUrl=environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class RegisterService {
  private _apiBaseUrl = apiUrl;
  constructor(private _http: HttpClient) { }
}
