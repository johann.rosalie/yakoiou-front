import { Injectable } from '@angular/core';
import { Subject } from 'rxjs'; 
import {Communes} from 'src/app/common/data/communes'

@Injectable({
  providedIn: 'root'
})
export class DataserviceService {
  SharingData = new Subject();
  communes =new Subject();
  Activite= new Subject();
  Codedepartement=new Subject();
  constructor() { }
}
