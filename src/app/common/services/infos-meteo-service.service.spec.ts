import { TestBed } from '@angular/core/testing';

import { InfosMeteoServiceService } from './infos-meteo-service.service';

describe('InfosMeteoServiceService', () => {
  let service: InfosMeteoServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(InfosMeteoServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
