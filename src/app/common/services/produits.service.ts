import { Injectable } from '@angular/core';
import {Produits} from 'src/app/common/data/produits';
import {Observable} from 'rxjs';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import {catchError} from 'rxjs/operators';
const apiUrl = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class ProduitsService {

  private _apiBaseUrl = environment.apiNodeUrl;
  private _headers = new HttpHeaders({'Access-Control-Allow-Origin':'*','Content-Type': 'application/json'  });
  constructor(private http: HttpClient) { }

  public getAllProduitsEnable$(): Observable<Produits[]> {
    let url = this._apiBaseUrl + "/admin/produits/listproduits";
    const headers=new HttpHeaders().set("Content-Type","application/json");
    return this.http.get<Produits[]>(url, { headers});
  }

  public updateProduit(produit:Produits):Observable<any>
  {
    let url = this._apiBaseUrl + "/admin/produits/majproduit";
    const headers=new HttpHeaders().set("Content-Type","application/json");
    return this.http.put<Produits[]>(url,produit, { headers});
  }
}
