import { TestBed } from '@angular/core/testing';

import { MeteodetailsService } from './meteodetails.service';

describe('MeteodetailsService', () => {
  let service: MeteodetailsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MeteodetailsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
