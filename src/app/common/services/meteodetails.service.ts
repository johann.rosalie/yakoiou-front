import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import {catchError} from 'rxjs/operators';
import {MeteoDetails} from '../data/meteodetails';
const  API_URL= environment.apiNodeUrl;

@Injectable({
  providedIn: 'root'
})
export class MeteoDetailsService {
  private _apiBaseUrl = API_URL;
  private _headers = new HttpHeaders({'Access-Control-Allow-Origin':'*','Content-Type': 'application/json'  });
  constructor(private http: HttpClient) { }
  getMeteoAllDetails$():Observable<MeteoDetails[]>
  {
    const headers=new HttpHeaders().set("Content-Type","application/json");
    return this.http.get<MeteoDetails[]>(API_URL+ '/dayly_weather/public/Alldayly_weathers',{ headers}).pipe(catchError(this.handleError));
  }
  getMeteoAllDetailsByCommune$(commune:string):Observable<MeteoDetails>{
    const headers=new HttpHeaders().set("Content-Type","application/json");
    return this.http.get<MeteoDetails>(API_URL+ '/dayly_weather/public/commune/'+ commune ,{ headers}).pipe(catchError(this.handleError));
  }
  private handleError(error: Response|any){
    return Observable.throw(error);
  }
}
