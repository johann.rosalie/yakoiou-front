import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject} from 'rxjs';
import { Login } from '../data/login'
import { map } from 'rxjs/operators';
import {  LoginResponse } from '../data/loginresponse';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { tap  } from 'rxjs/operators';
import { environment } from 'src/environments/environment';


const apiUrl=environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private _apiBaseUrl = apiUrl;
  private _headers = new HttpHeaders({'Access-Control-Allow-Origin':'*', 'Content-Type': 'application/json' });
  private isloggedIn: boolean;
  //private currentUserSubject: BehaviorSubject<LoginResponse>;
  //public currentUser: Observable<LoginResponse>;

  constructor(private _http: HttpClient) { this.isloggedIn=false;}

  public postLogin$(login: Login): Observable<LoginResponse> {
    let url = this._apiBaseUrl + "/api/auth/signin";
    return this._http.post<LoginResponse>(url,
      login,
      { headers: this._headers })
      .pipe(tap((loginresponse) => { this.sauvegarderJeton(loginresponse); }));
  }
  private sauvegarderJeton(loginResponse: LoginResponse): void {
    if (loginResponse.accessToken) {
      sessionStorage.setItem('authToken', loginResponse.accessToken);
      sessionStorage.setItem('Roles', loginResponse.roles[0]);
    }
    else {
      sessionStorage.setItem('authToken', null);
      sessionStorage.setItem('Roles', null);
    }
  }

  
  logout(): void{
    sessionStorage.removeItem('authToken');
    sessionStorage.removeItem('Roles');
    

  }
}
