import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { User } from '../data/user';
import { map } from 'rxjs/operators';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import {Register} from 'src/app/common/data/register';
const apiUrl = environment.apiUrl;
@Injectable({
  providedIn: 'root'
})
export class UserInfosService {
  private _apiBaseUrl = environment.apiUrl;
  private _headers = new HttpHeaders({ 'Access-Control-Allow-Origin': '*', 'Content-Type': 'application/json' });

  constructor(private _http: HttpClient) { }

  public getAllUserInfosService$(): Observable<User[]> {
    let url = this._apiBaseUrl + "/api/auth/listdesUtilisateurs";
    console.log("url = " + url);
    return this._http.get<User[]>(url, { headers: this._headers });
  }

  // /admin/DelteUtilisateur/{username}
  public deleteUserInfosService$(id:Number): Observable<any> {

    let url = this._apiBaseUrl + "/api/auth/admin/DelteUtilisateur/";
    
    return this._http.delete<String>(url + id, { headers: this._headers });

  }
  public UpdateUserInfosService$(user: User): Observable<any> {
    let url = this._apiBaseUrl + "/api/auth/public/modifierUtilisateur/";
    return this._http.put<User>(url, user, { headers: this._headers });
  }
  
  public RegisterUserInfosService$(register:Register): Observable<any> {
    let url = this._apiBaseUrl + "/api/auth/signup/";
    return this._http.post<User>(url, register, { headers: this._headers });
  }
  public RechercheUserByEmailInfosService$(email: String): Observable<any> {
    let url = this._apiBaseUrl + "/admin/rechercherUtilisateurUsername/";
    return this._http.get<String>(url + email, { headers: this._headers });

  }

}
