export class Communes {
    id:number;
    constructor(
    public code_commune_INSEE?: String,
    public nom_commune_postal?: String,
    public code_postal?: String,
    public libelle_acheminement?:String,
    public latitude?: String,
    public longitude?: String,
    public nom_commune?: String,
    public nom_commune_complet?: String,
    public code_departement?: String,
    public nom_departement?: String,
    public code_region?:String,
    public nom_region?: String){}
}
