import {Roles} from 'src/app/common/data/auth.enum';
export class Register {
    constructor( public firstname:String="" , 
        public lastname:String="" ,
        public username: string="",
        public email: string="",
        public password:String="",
        public roles: Roles=Roles.ROLE_USER ){}
}

