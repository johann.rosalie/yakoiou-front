import {Roles} from 'src/app/common/data/auth.enum';
export class User {
    
    constructor( public id : number,public firstname:String , public lastname:String,public username: string ,public email: string,public password:String,public roles: Roles[] ){}
    
}
