export interface Datatourisme {
    _id: Id;
    'rdfs:label': Rdfslabel;
    '@type': string[];
    hasClientTarget: HasClientTarget[];
    hasContact: HasContact[];
    hasDescription: HasDescription[];
    hasTheme: any[];
    isLocatedAt: IsLocatedAt[];
    offers: Offer[];
    COVID19OpeningPeriodsConfirmed: boolean;
   
   
    
  }
  
  interface Offer {
    'schema:priceSpecification': SchemapriceSpecification[];
  }
  
  interface SchemapriceSpecification {
  }
  
  interface IsLocatedAt {
    'schema:address': Schemaaddress[];
    'schema:geo': Schemageo;
    'schema:openingHoursSpecification': SchemaopeningHoursSpecification[];
  }
  
  interface SchemaopeningHoursSpecification {
    'schema:closes': string;
    'schema:opens': string;
  }
  
  interface Schemageo {
    'schema:latitude': string;
    'schema:longitude': string;
  }
  
  interface Schemaaddress {
    'schema:addressLocality': string;
    'schema:postalCode': string;
    'schema:streetAddress': string[];
  }
  
  interface HasDescription {
    shortDescription: Rdfslabel;
  }
  
  interface HasContact {
    'schema:email': string[];
    'schema:telephone': string[];
    'foaf:homepage': string[];
  }
  
  interface HasClientTarget {
    'rdfs:label': Rdfslabel;
  }
  
  interface Rdfslabel {
    fr: string[];
  }
  
  interface Id {
    '$oid': string;
  }