export class MeteoDetails {
    _id: Id;
    coord: Coord;
    main: Main;
    wind: Wind;
    clouds: Clouds;
    weather: Weather[];
    num_carto: number;
    name: string;
    dt: number;
    rain?: any;
    snow?: any;
    __v: number;
  }
  
  interface Weather {
    id: number;
    main: string;
    description: string;
    icon: string;
  }
  
  interface Clouds {
    all: number;
  }
  
  interface Wind {
    speed: number;
    deg: number;
  }
  
  interface Main {
    temp: number;
    feels_like: number;
    temp_min: number;
    temp_max: number;
    pressure: number;
    humidity: number;
  }
  
  interface Coord {
    lat: number;
    lon: number;
  }
  
  interface Id {
    '$oid': string;
  }