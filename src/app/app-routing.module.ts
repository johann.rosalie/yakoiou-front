import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LoginComponent} from './login/login.component';
import { MapComponent} from '../app/map/map.component';
import {UsersinfosComponent} from 'src/app/usersinfos/usersinfos.component';
import { HomeComponent } from './home/home.component';
import {RegisterComponent}from 'src/app/register/register.component'
import {UserProfileComponent} from 'src/app/user-profile/user-profile.component';
 import{ManagerComponent} from 'src/app/manager/manager.component';
 import{LogoutComponent} from 'src/app/logout/logout.component';
 import{ActivitiestourComponent} from 'src/app/activitiestour/activitiestour.component';
 import {ProduitstourComponent} from 'src/app/produitstour/produitstour.component';
 import {ActititydetailsComponent} from 'src/app/actititydetails/actititydetails.component';
import { DatatourismeService } from './common/services/datatourisme.service';
import {Datatourisme} from 'src/app/common/data/datatourisme';

const routes: Routes = [{path: '',component:HomeComponent },
{path: 'ngr-login', component: LoginComponent},
{path: 'ngr-listusers',component:UsersinfosComponent },
{path: 'ngr-register',component: RegisterComponent},
{path:'nrg-logout',component:LogoutComponent},
{path: 'ngr-userprofile/:id', component:UserProfileComponent},
{path:'ngr-activitiestour',component:ActivitiestourComponent},
{ path: '', redirectTo:  'ngr-home', pathMatch: 'full'},
{path: 'ngr-map', component: MapComponent},
{path: 'ngr-activitydetails',component: ActititydetailsComponent},
{path:'ngr-Produitstour',component:ProduitstourComponent},
{path: 'ngr-manager', component: MapComponent,
children:[{path: 'ngr-listusers',component:UsersinfosComponent }]},
{ path: '**', redirectTo: '' }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
