import { Component, Input } from '@angular/core';
import {AuthService} from '../../common/service/auth.service';
import {Observable} from 'rxjs';
import {debounceTime, distinctUntilChanged, map} from 'rxjs/operators';
import{ Communes} from 'src/app/common/data/communes';
import {DataServiceService} from 'src/app/common/service/data-service.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  //public model: any;
  //@Input() commune:  Communes;
  isLogin:boolean=false;
  commune:  Communes;
  constructor(private DataSharing: DataServiceService) {
  this.DataSharing.SharingData.subscribe((res: any) => {  
    this.commune = res;  
  });
}
onSubmit(data) {  
  this.DataSharing.SharingData.next(data.value);  
}  
  /* formatter = (result: string) => result.toUpperCase();

  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      map(term => term === '' ? []
        : commune.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
    ) */
 
    logout(){}
}
