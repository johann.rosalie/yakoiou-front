import { Component, OnInit } from '@angular/core';
import { User} from '../common/data/user';
import {UserInfosService} from 'src/app/common/services/userinfos.service';
import { Observable, of } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-usersinfos',
  templateUrl: './usersinfos.component.html',
  styleUrls: ['./usersinfos.component.scss']
})
export class UsersinfosComponent implements OnInit {
  selectedUser:User;
  listUserInfos : User[];
  listeroles :string[];
 constructor(private _userInfosService : UserInfosService, private route: ActivatedRoute,
  private router: Router ) {   
  }
   initListUser(tabUser : User[]){
     this.listUserInfos=tabUser;
   }

 ngOnInit(): void {
   this._userInfosService.getAllUserInfosService$()
   .subscribe({
     next: (tabUser: User[])=>{ this.initListUser(tabUser); },
     error: (err) => { console.log("error:"+err)}
  });
}
/* affiche(listRoles:any[]){
 let ltRoles:String="";
  listRoles.forEach(element => {
    ltRoles=ltRoles+' '+element.name;
  });
  return ltRoles.split("ROLE_").join("");
} */
selectuser(user:User){ this.selectedUser=user;}
    
}