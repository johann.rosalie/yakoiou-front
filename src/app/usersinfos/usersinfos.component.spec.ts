import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersinfosComponent } from './usersinfos.component';

describe('UsersinfosComponent', () => {
  let component: UsersinfosComponent;
  let fixture: ComponentFixture<UsersinfosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UsersinfosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersinfosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
