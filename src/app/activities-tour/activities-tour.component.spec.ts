import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivitiesTourComponent } from './activities-tour.component';

describe('ActivitiesTourComponent', () => {
  let component: ActivitiesTourComponent;
  let fixture: ComponentFixture<ActivitiesTourComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActivitiesTourComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivitiesTourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
