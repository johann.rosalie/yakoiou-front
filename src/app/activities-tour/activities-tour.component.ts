import { Component, OnInit } from '@angular/core';
import {DatatourismeService} from '../common/service/datatourisme.service';
import {DataServiceService} from '../common/service/data-service.service';
import {Datatourisme } from 'src/app/common/data/datatourisme';

@Component({
  selector: 'app-activities-tour',
  templateUrl: './activities-tour.component.html',
  styleUrls: ['./activities-tour.component.scss']
})
export class ActivitiesTourComponent implements OnInit {
  _ActivityCommune:Datatourisme[];
  communeSelect:String;
  typeActivite:String="SportsAndLeisurePlace";
  codePostal:String="84000";
  communetest:String="Montpellier";
  codeDepartement:String="38";
  constructor(private api:DatatourismeService ,private DataSharing: DataServiceService  ){
    this.DataSharing.SharingData.subscribe((res: any) => {  
      if (res){
      this.communeSelect = res;  }
    });
  }

 

  getActivityByCommune(nomCommune:String, typeActivite:String,codePostal:String) {
    this.api.getActivityTypeByCommnune$(nomCommune, typeActivite,codePostal).subscribe(
       (datatour: Datatourisme[])=>{  this._ActivityCommune= datatour; }
    
   );
    }
   getActivitiesByDepartetement(codeDepartement:String, typeActivite:String) {
    this.api.getActivitiesTypeByDepartement$(codeDepartement,typeActivite).subscribe(
       (datatourDep: Datatourisme[])=>{  this._ActivityCommune= datatourDep; }
    
   );

}
ngOnInit(){
    //this.getActivityByCommune(this.communetest, this.typeActivite,this.codePostal);
    this.getActivitiesByDepartetement(this.codeDepartement, this.typeActivite);
  }
}
