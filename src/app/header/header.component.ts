import { Component,  OnInit, DoCheck, ChangeDetectionStrategy } from '@angular/core';

import {Observable} from 'rxjs';
import {debounceTime, distinctUntilChanged, map} from 'rxjs/operators';
import{ Communes} from 'src/app/common/data/communes';
import {DataserviceService} from 'src/app/common/services/dataservice.service';
import {CommunesserviceService} from 'src/app/common/services/communes.service';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead/typeahead-match.class'; 
import{AuthService} from 'src/app/common/services/authservice.service';
import { Router, ActivatedRoute } from '@angular/router';




@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  changeDetection: ChangeDetectionStrategy.Default
})
export class HeaderComponent  implements OnInit{
  //public model: any;
  //@Input() commune:  Communes;
  _lstVilles:Communes[];
  isLogin:Boolean=false;
  usrRole:Boolean=false;
  commune: String;
  communesDetails:Communes;
  
  private apiAuth:AuthService;
  
  constructor( private DataSharing: DataserviceService, private apiCommune:CommunesserviceService, private route: ActivatedRoute,
    private router: Router) {
  this.DataSharing.SharingData.subscribe((res: any) => {  
    this.commune = res;  
  });/*
  this.DataSharing.communes.subscribe((res: any) => {  
    this.communesDetails = res;  
  });*/
}
onSubmit(data) {  
 this.DataSharing.SharingData.next(data.value); 
 this.getDetailsAllCommunesMenu(data.value);
 
 this.DataSharing.communes.next(this.communesDetails);
 this.majChanges();
 //console.log(this.commune);
}  

getDetailsAllCommunesMenu(villes:String) {
  this.apiCommune.getDetailsCommuneByName$(villes).subscribe(
     (infosCommunes: Communes)=>{  this.communesDetails= infosCommunes; }
  
 );
}
majChanges(){
  this.isLogin=this.getInfoslogin();
  this.usrRole=this.getInfosRoles();
  const returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  this.router.navigateByUrl(returnUrl);
}
getInfosRoles():Boolean{
let Roles = sessionStorage.getItem('Roles');
if (Roles=="ROLE_ADMIN"){return true;}
  else{return false;} 
}
getInfoslogin(): Boolean{
  let token = sessionStorage.getItem('authToken');
  
  if(token){return true;  }
  else {return false;}
  
}
ngOnInit(){
  //this.getDetailsAllCommunesMenu();
  this.isLogin=this.getInfoslogin();
  this.usrRole=this.getInfosRoles();
}


    logout(){
      sessionStorage.removeItem('authToken');
      sessionStorage.removeItem('Roles');
      const returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
      this.router.navigateByUrl(returnUrl);
    }
  }
    
