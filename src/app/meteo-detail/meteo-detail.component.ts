import { Component, OnInit,OnChanges } from '@angular/core';
import {MeteoDetailsService} from '../common/service/meteo-details.service';
import {  MeteoDetails} from '../common/data/meteo-details';
//import {FormsModule} from '@angular/forms';
import { ObservedValuesFromArray } from 'rxjs';
import {DataServiceService} from "../common/service/data-service.service";
//import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-meteo-detail',
  templateUrl: './meteo-detail.component.html',
  styleUrls: ['./meteo-detail.component.scss']
})
export class MeteoDetailComponent implements OnInit,OnChanges {
 //public myMeteoDetails= new MeteoDetails("Avignon");
 communeSelect:String="";
  constructor(private api:MeteoDetailsService ,private DataSharing: DataServiceService  ){
    this.DataSharing.SharingData.subscribe((res: any) => {  
      this.communeSelect = res;  
      this.getMeteoByCommune(this.communeSelect);
    });
  }
  ngOnInit(): void {
    this.getMeteoByCommune(this.communeSelect);
  }
 // _MeteoCommune:any;// new MeteoDetails();
  _MeteoCommune = new MeteoDetails();

   getMeteoByCommune(communes) {
    this.api.getMeteoAllDetailsByCommune$(communes).subscribe(
       (datameteo: MeteoDetails)=>{  this._MeteoCommune= datameteo; }
    
   );
      
  }
  ngOnChanges(): void {
    this.getMeteoByCommune(this.communeSelect);
  }
 
}
 