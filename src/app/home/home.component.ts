import { Component, OnInit } from '@angular/core';
import {AuthService} from 'src/app/common/services/authservice.service';
import {DataserviceService} from 'src/app/common/services/dataservice.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  communeSelect:String;
  constructor(private DataSharing: DataserviceService ,private authservice:AuthService){
    this.DataSharing.SharingData.subscribe((res: any) => {  
      this.communeSelect = res;  });
    }

  ngOnInit(): void {
  }

}
