import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InfosMeteoComponent } from './infos-meteo.component';

describe('InfosMeteoComponent', () => {
  let component: InfosMeteoComponent;
  let fixture: ComponentFixture<InfosMeteoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InfosMeteoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InfosMeteoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
