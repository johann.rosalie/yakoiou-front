import { Component } from '@angular/core';
import {Login} from 'src/app/common/data/login';
import{AuthService} from 'src/app/common/services/authservice.service';
import {LoginResponse} from 'src/app/common/data/loginresponse';
import { Router, ActivatedRoute, RouterLink } from '@angular/router';
import { Location } from '@angular/common';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  
  loading = false;
  submitted = false;
  error = '';
  public  login = new Login();
 public message: string | undefined;
 constructor(private _AuthService: AuthService, private route: ActivatedRoute,
  private router: Router,private location:Location) { }
 
  onLogin():void{
    
    this._AuthService.postLogin$(this.login).subscribe({
      next: (response:LoginResponse): void => { this.message = JSON.stringify(response);/* alert("Success") ; */const returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
      this.router.navigateByUrl(returnUrl); },
      error: (err) => {this.message = err;/* alert("error : " + err); */ this.error = "Error: login or password error! Please try again";
      this.loading = false; const returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/ngr-login';
      this.router.navigateByUrl(returnUrl);}
    });
  }
  goBack(): void {
    this.location.back();
  }
}
