import { Component, OnInit,OnChanges, Input } from '@angular/core';
import { MeteoDetailsService} from 'src/app/common/services/meteodetails.service';
import {  MeteoDetails} from '../common/data/meteodetails';
//import {FormsModule} from '@angular/forms';
import { ObservedValuesFromArray } from 'rxjs';
import {DataserviceService } from "../common/services/dataservice.service";
//import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-meteodetails',
  templateUrl: './meteodetails.component.html',
  styleUrls: ['./meteodetails.component.scss']
})
export class MeteodetailsComponent implements OnInit {
 
//public myMeteoDetails= new MeteoDetails("Avignon");
@Input() communeSelect:String;
constructor(private api: MeteoDetailsService /* ,private DataSharing: DataserviceService */ ){
  /* this.DataSharing.SharingData.subscribe((res:any) => {  
    this.communeSelect = res;  
    this.getMeteoByCommune(this.communeSelect);}); */
  
}
ngOnInit(): void {
  this.getMeteoByCommune(this.communeSelect);
}
// _MeteoCommune:any;// new MeteoDetails();
_MeteoCommune = new MeteoDetails();

 getMeteoByCommune(communes) {
  this.api.getMeteoAllDetailsByCommune$(communes).subscribe(
     (datameteo: MeteoDetails)=>{  this._MeteoCommune= datameteo; }
  
 );
    
}
ngOnChanges(): void {
  this.getMeteoByCommune(this.communeSelect);
}


}
