import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MeteodetailsComponent } from './meteodetails.component';

describe('MeteodetailsComponent', () => {
  let component: MeteodetailsComponent;
  let fixture: ComponentFixture<MeteodetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MeteodetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MeteodetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
