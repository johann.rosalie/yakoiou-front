import { Component, OnInit,Input } from '@angular/core';
import {Datatourisme } from 'src/app/common/data/datatourisme';
import {DatatourismeService} from 'src/app/common/services/datatourisme.service';
import {DataserviceService}from 'src/app/common/services/dataservice.service';
import { Communes } from '../common/data/communes';
import { ActivatedRoute ,Router} from '@angular/router';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-activitiestour',
  templateUrl: './activitiestour.component.html',
  styleUrls: ['./activitiestour.component.scss']
})
export class ActivitiestourComponent implements OnInit {
  //@Input() 
   typeActivite:String;
//@Input codeDepartement:String;
  _ActivityCommune:Datatourisme[];
 // @Input()
    communeSelect:String;
    activityselect:Datatourisme;
    queryParams = ['typeActivite'];
  codeDepartement:String;
  message:String;
  totalItems = 64;
  currentPage = 4;
 
  setPage(pageNo: number): void {
    this.currentPage = pageNo;
  }
  constructor(private api:DatatourismeService  ,private DataSharing: DataserviceService,private route: ActivatedRoute ,private router : Router ){
     this.DataSharing.SharingData.subscribe((res: any) => {  
      if (res){
      this.communeSelect = res;  }
    });
    this.DataSharing.Activite.subscribe((res: any) => {  
      if (res){
      this.typeActivite = res;  }
    }); 
    this.DataSharing.Codedepartement.subscribe((res: any) => {  
      if (res){
      this.codeDepartement = res;  
      this.getActivitiesByDepartetement(this.codeDepartement, this.typeActivite);
    }
    });
  }

 

  getActivityByCommune(nomCommune:String, typeActivite:String,codePostal:String) {
    this.api.getActivityTypeByCommnune$(nomCommune, typeActivite,codePostal).subscribe(
       (datatour: Datatourisme[])=>{  this._ActivityCommune= datatour; }
    
   );
    }
   getActivitiesByDepartetement(codeDepartement:String, typeActivite:String) {
    this.api.getActivitiesTypeByDepartement$(codeDepartement,typeActivite).subscribe(
     
   {next: (datatourDep:Datatourisme[]): void => { this._ActivityCommune = (datatourDep);
  },
   error: (err) => { console.log("error : " + err); }
 });
}
ngOnInit(){
  this.route.queryParams
  .subscribe((params) => {
    console.log(params.get('typeActivite'));
    this.typeActivite = params['typeActivite'];
  });
  

  /* this.DataSharing.SharingData.subscribe((res: any) => {  
    if (res){
    this.communeSelect = res;  }
  });
  this.DataSharing.Activite.subscribe((res: any) => {  
    if (res){
    this.typeActivite = res;  }
  });
  this.DataSharing.communes.subscribe((res: Communes) => {  
    if (res){
    this.codeDepartement = res[0].code_departement;  
    console.log(res);
    //this.getActivitiesByDepartetement(this.codeDepartement, this.typeActivite);
  }
  }); */
    this.getActivitiesByDepartetement(this.codeDepartement, this.typeActivite);
  }

  
  selectedtour(activity: Datatourisme ){this.router.navigateByUrl('/ngr-activitydetails', { state: activity });

  }
afficheDescriptions(data:any):String{
let  deserialise=data[0].shortDescription.fr[0];

return deserialise;
}


}
