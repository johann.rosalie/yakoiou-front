import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivitiestourComponent } from './activitiestour.component';

describe('ActivitiestourComponent', () => {
  let component: ActivitiestourComponent;
  let fixture: ComponentFixture<ActivitiestourComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActivitiestourComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivitiestourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
