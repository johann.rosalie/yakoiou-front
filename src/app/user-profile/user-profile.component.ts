import { Component, Input, OnInit, TemplateRef } from '@angular/core';  
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';  
import { User } from '../common/data/user';
import {UserInfosService} from 'src/app/common/services/userinfos.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';


@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {
 
 @Input() user:User;
  constructor( private apiUserService:UserInfosService,private route:ActivatedRoute,private location: Location) {}

  ngOnInit(): void {
  }

  goBack(): void {
    this.location.back();
  }

  saveUser(): void {
    this.apiUserService.UpdateUserInfosService$(this.user)
      .subscribe((res) =>{alert(res); this.goBack()});
  }
   affiche(listRoles:any[]){
    let ltRoles:String="";
     listRoles.forEach(element => {
       ltRoles=ltRoles+' '+element.name;
     });
     return ltRoles.split("ROLE_").join("");
   }
  deleteUser(): void {
    this.apiUserService.deleteUserInfosService$(this.user.id)
      .subscribe((res) =>{alert(res); this.goBack()});
  }
}

